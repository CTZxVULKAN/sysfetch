<div align="center">

<img src="https://user-images.githubusercontent.com/65299153/135719227-52adf365-d619-4b1f-b5fe-8aa2bbd25c56.png" width="60" />

<h1 align="center">Sysfetch</h1>
<p align="center">Yet another fetch script for linux based systems.</p>

<div align="center">
 
 ![Platform](https://img.shields.io/static/v1?label=Language&message=Python&style=for-the-badge&logo=Python)
 ![Platform](https://img.shields.io/static/v1?label=Platform&message=Linux&style=for-the-badge&logo=tmux)
 ![Platform](https://img.shields.io/static/v1?label=Stage&message=Alpha&style=for-the-badge&logo=fastapi)
 
</div>

</div>


## What is this and what this isn't ? 
Unlike a lot of fetch scripts out there sysfetch isn't focused on displaying your host-info, uptime, packages, themes etc. 

Sysfetch rather focuses on displaying *system stats*. 
The aim of the project is to display system stats like *CPU load, memory useage, swap useage, etc* in a clean manner.

The project follows the *Suckless philosophy* of keeping things simple, minimal and usable. The use of external libraries and modules is avoided as much as possible.

Unlike other suckless projects this has been written in Python instead of C for easy readibility and to encourage new users to experement with the code.

## Installation
This project is still in **alpha** stage. It is not recommended to install this application.

## Usage
```
./sysfetch 
```


## Roadmap

* Add support for config files.
* Add proper documentaion for the code.
* Add live mode to view stats in realtime.


## License
This project has been licensed under the **MIT license**.
___
MIT License

Copyright (c) 2021 CTZ VULKAN

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



